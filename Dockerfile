FROM node:17.1

#Prod env
ENV NODE_ENV=production

# Create work dir for the app
WORKDIR /usr/app/

#Copy package to app
COPY package*.json ./

#copy app files
COPY src src
COPY config.json config.json

#Install dependancies
RUN npm install --production

#Expose HTTP port
EXPOSE 80
#Expose API port
EXPOSE 8080

#Start the App
CMD [ "npm", "start" ]
