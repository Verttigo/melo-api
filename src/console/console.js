const readLine = require("readline"),
    meloAPI = require("../index"),
    rl = readLine.createInterface({
        "input": process.stdin,
        "output": process.stdout
    }),
    cluster = require("../cluster/cluster");


// Thanks Eric on StackOverflow !
fixStdoutFor = (cli) => {
    let oldStdout = process.stdout,
        newStdout = Object.create(oldStdout);

    newStdout.write = function() {
        cli.output.write("\x1b[2K\r");
        let result = oldStdout.write.apply(
            this,
            Array.prototype.slice.call(arguments)
        );

        cli._refreshLine();
        return result;
    };
    process.__defineGetter__("stdout", function() { return newStdout; });
};

exports.start = (uuid) => {
    fixStdoutFor(rl);
    rl.setPrompt("Manager: ");
    rl.prompt();
    let clusterData;

    rl.on("line", async(input) => {
        switch (input) {
            case "quit":
                console.log("Melo-API : Shutting now, delegating roles if i'm important :(");
                await meloAPI.exit();
                break;
            case "info":
                clusterData = await cluster.getCluster();
                console.log(`This node is ${uuid}`);
                console.log(`Role : ${ cluster.getNodeRole(clusterData)}`);
                break;
            case "cluster nodes":
                clusterData = await cluster.getCluster();
                let index = 1;

                console.log(`Master : ${cluster.getMaster(clusterData)}`);
                for (const worker of cluster.getWorkers(clusterData)) {
                    console.log(`Node ${index} : ${worker.UUID}`);
                    index++;
                }
                break;
            case "cluster destroy" :
                await cluster.destroyCluster(uuid);
                process.exit(1);
                break;
            case "":
                break;
            case "help":
                console.log("quit : to quit the app (Thanks Captain)");
                console.log("info : info about this node");
                console.log("cluster nodes : show nodes in the cluster");
                console.log("cluster destroy : shutdown the cluster");
                break;
            default:
                console.log("Uknown command, type help to see commands available");
        }
        rl.prompt();
    });
};

exports.stop = () => {
    rl.close();
};
