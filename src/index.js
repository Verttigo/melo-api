const conf = require("../config.json"),
    { "v4": uuidv4 } = require("uuid");

// eslint-disable-next-line no-unused-vars
let cluster, nodeID, cWorker, cMaster, RedisInstance, proxy, meloConsole;


init = async () => {
    // UUID
    nodeID = await uuidv4(undefined, undefined, undefined);

    // Console
    meloConsole = require("./console/console");
    meloConsole.start(nodeID);

    // INIT
    console.log("-------- Melo-API --------");
    console.log(`Melo-API : Node UUID: ${nodeID}`);
    console.log("Melo-API : Trying to connect to Redis Server...");
    RedisInstance = require("./databases/redis");
    if (conf.cluster.enable) {
        console.log("Melo-API : This instance is running in cluster");

        // DB
        await RedisInstance.connect();
        await RedisInstance.listen(nodeID);

        // Cluster
        cluster = require("./cluster/cluster");
        await cluster.initCluster(nodeID);
        cWorker = require("./cluster/worker/cworker");
        cMaster = require("./cluster/master/cmaster");

    } else {
        console.log("Melo-API :  Running in standalone");
    }
    // HTTP
    proxy = require("./proxy/routes");
    await proxy.start();
};

exports.exit = async () => {
    meloConsole.stop();
    if (conf.cluster.enable && cluster !== undefined) {
        await cluster.quit(nodeID);
    }
    console.log("Melo-API : Bye Bye");
    process.exit(1);
};

init().then(() => console.log("Melo-API : Init completed"));

process.on("SIGINT", () => this.exit());

