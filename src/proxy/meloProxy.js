const undici = require("undici");


filterHeaders = (headers, filter) => {
    const headersKeys = Object.keys(headers),
        dest = {};

    let header;

    for (let i = 0; i < headersKeys.length; i++) {
        header = headersKeys[ i ].toLowerCase();
        if (header !== filter) {
            dest[ header ] = headers[ header ];
        }
    }
    return dest;
};


buildRequest = (opts) => {
    const baseUrl = opts.base,
        pool = new undici.BalancedPool(baseUrl, opts.undici);

    if (typeof opts.undici !== "object") {
        opts.undici = {};
    }

    let handleUndici = (reqParams, done) => {
        const req = {
            "origin": reqParams.origin,
            "path": reqParams.path,
            "headers": reqParams.headers,
            "method": reqParams.method,
            "body": reqParams.body
        };

        pool.request(req, function (err, res) {
            if (err) {
                done(err);
            } else {
                done(null, { "statusCode": res.statusCode, "headers": res.headers, "body": res.body });
            }
        });
    };

    return { "request": handleUndici };


};

meloProxy = (options = {}) => {
    const { request } = buildRequest(options);

    return {
        proxy(req, res, opts = {}) {
            const onResponse = opts.onResponse;

            let originalHeaders = req.headers,
                bodyReq = null,
                reqParams;

            delete originalHeaders.connection;

            if (req.method === "GET" || req.method === "HEAD") {
                if (originalHeaders[ "content-length" ]) {
                    originalHeaders = filterHeaders(originalHeaders, "content-length");
                } else if (req.body) {
                    if (originalHeaders[ "content-type" ] === "application/json") {
                        bodyReq = JSON.stringify(req.body);
                    } else {
                        res.statusCode = 503;
                        res.send("Unsupported application type");
                    }
                }
            }

            reqParams = {
                "method": req.method,
                "headers": originalHeaders,
                bodyReq,
                "origin": req.protocol + req.hostname + req.url,
                "path": req.url
            };

            request(reqParams, (err, response) => {
                if (err) {
                    if (!res.sent) {
                        if (err.code === "ECONNREFUSED" || err.code === "ERR_HTTP2_STREAM_CANCEL") {
                            res.statusCode = 503;
                            res.send("Service Unavailable");
                        } else if (err.code === "ECONNRESET" || err.code === "UND_ERR_HEADERS_TIMEOUT" || err.code === "UND_ERR_BODY_TIMEOUT") {
                            res.statusCode = 504;
                            res.send(err.message);
                        } else {
                            res.statusCode = 500;
                            res.send(err.message);
                        }
                    }

                    return;
                }

                // destructing response from remote
                const { statusCode, body, headers } = response;

                res.headers(originalHeaders);
                res.header("x-forwarded-host", req.hostname);
                res.header("host", headers.dispatch);

                // set origin response code
                res.statusCode = statusCode;
                res.send(body);

                if (onResponse) {
                    onResponse(req, res, body);
                }
            });
        }
    };
};

module.exports = meloProxy;
