require("../index");
const Redis = require("ioredis"),
    cWorker = require("../cluster/worker/cworker"),
    cMaster = require("../cluster/master/cmaster"),
    conf = require("./../../config.json"),
    subscriber = new Redis({
        "port": conf.redis.port,
        "host": conf.redis.host,
        "password": conf.redis.auth,
        "db": conf.redis.slot
    }),
    publisher = new Redis({
        "port": conf.redis.port,
        "host": conf.redis.host,
        "password": conf.redis.auth,
        "db": conf.redis.slot
    }),
    redis = new Redis({
        "port": conf.redis.port,
        "host": conf.redis.host,
        "password": conf.redis.auth,
        "db": conf.redis.slot
    });


exports.publish = (channel, message) => {
    publisher.publish(channel, message);
};

exports.subscribe = (channel) => {
    subscriber.subscribe(channel, (err) => {
        if (err) {
            console.error("Melo-API : Failed to subscribe: %s", err.message);
        }
    });
};

exports.unSubscribe = (channel) => {
    subscriber.unsubscribe(channel, (err) => {
        if (err) {
            console.error("Melo-API : Failed to subscribe: %s", err.message);
        }
    });
};

exports.getClusterInfos = async() => {
    return new Promise((resolve) => {
        redis.get("clusterInfos", (err, result) => {
            if (err) {
                console.error(err);
            } else {
                resolve(JSON.parse(result));
            }
        });
    });
};

exports.setClusterInfos = async (clusterInfos) => {
    await redis.set("clusterInfos", JSON.stringify(clusterInfos));
};

exports.delClusterInfos = async () => {
    await redis.del("clusterInfos");
};

exports.listen = (uuid) => {
    subscriber.on("message", async (channel, message) => {
        if (channel === uuid) {
            if (message.includes("remove")) {
                cMaster.removeHeartbeat(JSON.parse(message).remove);
            }
            if (message.includes("setMaster")) {
                console.log("Melo-API : I'm the new master");
                cWorker.stop();
                cMaster.init();
            }
            if (message.includes("setNextInLine")) {
                console.log("Melo-API : I'm the new NextInLine");
                await cWorker.init(uuid);
            }
            if (message.includes("Quit")) {
                console.log("Melo-API : Shutdown signal, stopping now");
                process.exit(1);
            }
        }
    });
};

exports.connect = async() => {
    return new Promise((resolve) => {
        let node = 0;

        redis.on("ready", () => {
            node++;
            console.log("Melo-API : Redis Transaction Instance is ready");
            if(node === 3) { resolve(); }
        });
        subscriber.on("ready", () => {
            node++;
            console.log("Melo-API : Redis Subscriber Instance is ready");
            if(node === 3) { resolve(); }
        });
        publisher.on("ready", () => {
            node++;
            console.log("Melo-API : Redis Publisher Instance is ready");
            if(node === 3) { resolve(); }
        });
        redis.on("error", (err) => {
            console.log(`Melo-API : Redis Transaction : ${err}`);
            process.exit(0);
        });
        subscriber.on("error", (err) => {
            console.log(`Melo-API : Redis Subscriber : ${err}`);
            process.exit(0);
        });
        publisher.on("error", (err) => {
            console.log(`Melo-API : Redis Publisher : ${err}`);
            process.exit(0);
        });
    });
};

exports.redis = redis;
exports.subscriber = subscriber;
