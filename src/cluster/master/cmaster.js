const RedisInstance = require("./../../databases/redis"),
    cluster = require("./../cluster"),
    utils = require("./../../utils/utils");

let workers = new Map(),
    heartbeatLoop = 0,
    verifyLoop = 0;

exports.init = () => {
    RedisInstance.subscribe("workers_heartbeat");
    heartbeatLoop = utils.setIntervalAndExecute(() => RedisInstance.publish("master_heartbeat", ""), 5000);
    verifyLoop = setInterval(async() => {
        for (const worker of workers.keys()) {
            let lastSeen = workers.get(worker);

            if (Date.now() - lastSeen > 10 * 1000) {
                console.log(`Melo-API : Worker : ${worker} has died, removing from cluster`);
                let clusterData = await cluster.getCluster(),
                    workersList = cluster.getWorkers(clusterData);

                if (cluster.isNextInLine(clusterData, worker) && workersList.length > 1) {
                    cluster.setNextInLine(clusterData.workers[ 1 ].UUID);
                }
                workers.delete(worker);
                await cluster.removeWorker(clusterData, worker);
            }
        }
    }, 5000);

    RedisInstance.subscriber.on("message", (channel, message) => {
        if (channel === "workers_heartbeat") {
            let worker = JSON.parse(message);

            if (!workers.has(worker.UUID)) {
                console.log(`Melo-API : Worker ${worker.UUID} has join the cluster`);
            }
            workers.set(worker.UUID, Date.now());
        }
    });
};

exports.removeHeartbeat = (uuid) => {
    workers.delete(uuid);
    console.log(`Melo-API : Removed Worker : ${uuid}`);
};

exports.stop = () => {
    RedisInstance.unSubscribe("workers_heartbeat");
    clearInterval(heartbeatLoop);
    clearInterval(verifyLoop);
};
