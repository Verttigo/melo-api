const Redis = require("../databases/redis"),
    cMaster = require("./master/cmaster"),
    cWorker = require("./worker/cworker"),
    conf = require("./../../config.json");


let nodeID;

exports.createCluster = async (uuid) => {
    await Redis.setClusterInfos({
        "master": uuid,
        "workers": []
    });
};

exports.addWorker = async (uuid, host, port, priority) => {
    let clusterInfo = await this.getCluster();

    clusterInfo.workers.push({
        "UUID": uuid,
        "host": host,
        "port": port,
        "priority": priority
    });
    await Redis.setClusterInfos(clusterInfo);
};

exports.removeWorker = async (cluster, uuid) => {
    let tempWorker = [];

    for (const worker of cluster.workers) {
        if (uuid !== worker.UUID) {
            tempWorker.push(worker);
        }
    }
    cluster.workers = tempWorker;
    await Redis.setClusterInfos(cluster);
    Redis.publish(cluster.master, JSON.stringify({ "remove": uuid }));
};

exports.getWorkers = (cluster) => {
    return cluster.workers ?? null;
};

exports.getCluster = async () => {
    return await Redis.getClusterInfos();
};

exports.getMaster = (cluster) => {
    return cluster.master ?? undefined;
};

exports.removeCluster = async () => {
    await Redis.delClusterInfos();
};

exports.isMaster = (cluster, uuid) => {
    return cluster.master === uuid ?? false;
};

exports.getNodeRole = (cluster) => {
    if (this.isThisNodeMaster(cluster)) {
        return "Master";
    }
    return "Worker";
};

exports.isThisNodeMaster = (cluster) => {
    return cluster.master === nodeID ?? false;
};

exports.isNextInLine = (cluster, uuid) => {
    return cluster.workers[ 0 ].UUID === uuid ?? false;
};

exports.getNextInLine = (cluster) => {
    return cluster.workers[ 0 ].UUID ?? false;
};

// ONLY FOR NextInLine
exports.setMaster = async (cluster) => {
    cluster.master = this.getNextInLine(cluster);
    cluster.workers.shift();
    await Redis.setClusterInfos(cluster);
};

exports.setNextInLine = (uuid) => {
    Redis.publish(uuid, JSON.stringify({ "setNextInLine": true }));
};

exports.getWorkerByUUID = (clusterData, uuid) => {
    for(const worker of clusterData.workers) {
        if(worker.UUID === uuid) { return worker; }
    }
};

exports.destroyCluster = async (uuid) => {
    let clusterData = await this.getCluster(),
        master = await this.getMaster(clusterData),
        workers = await this.getWorkers(clusterData);

    this.removeCluster();
    if (uuid !== master) {
        Redis.publish(master, JSON.stringify({ "Quit": true }));
    }
    for (const worker of workers) {
        if (worker.UUID !== uuid) {
            Redis.publish(worker.UUID, JSON.stringify({ "Quit": true }));
        }
    }
};

exports.initCluster = (uuid) => {
    return new Promise(async (done) => {
        nodeID = uuid;
        Redis.subscribe(uuid);
        if (await this.getCluster() === null) {
            await this.createCluster(uuid).then(() => {
                console.log("Melo-API : Cluster created, waiting for worker");
                cMaster.init();
                done();
            });
        } else {
            await this.addWorker(uuid, conf.melo.host, conf.melo.port, conf.melo.priority).then(() => {
                console.log(`Melo-API : Worker ${uuid} joined the cluster`);
                cWorker.init(uuid);
                done();
            });
        }
    });
};

exports.quit = async (uuid) => {
    const clusterData = await this.getCluster(),
        master = this.getMaster(clusterData),
        workers = this.getWorkers(clusterData);

    if (master === uuid) {
        if (workers.length === 0) {
            await this.removeCluster();
        } else {
            Redis.publish(workers[ 0 ].UUID, JSON.stringify({ "setMaster": true }));
            this.setMaster(clusterData);
            cMaster.stop();
        }
    } else {
        if (workers.length > 1 && this.getNextInLine(clusterData) === uuid) {
            cWorker.stop();
            this.setNextInLine(workers[ 1 ].UUID);
        }
        await this.removeWorker(clusterData, uuid);
    }
};

