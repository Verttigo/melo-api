const RedisInstance = require("./../../databases/redis"),
    utils = require("./../../utils/utils"),
    cluster = require("./../cluster"),
    cMaster = require("../master/cmaster");

let lastSeen = Date.now(), heartbeatLoop = 0, verifyLoop = 0;

exports.init = async(uuid) => {
    let clusterData = await cluster.getCluster();

    if(heartbeatLoop === 0) { heartbeat(clusterData, uuid); }
    if (cluster.isNextInLine(clusterData, uuid)) { verification(uuid); }

};

heartbeat = (clusterData, uuid) => {
    heartbeatLoop = utils.setIntervalAndExecute(() => {
        RedisInstance.publish("workers_heartbeat", JSON.stringify(cluster.getWorkerByUUID(clusterData, uuid)));
    }, 5000);
};

verification = (uuid) => {
    RedisInstance.subscribe("master_heartbeat");
    RedisInstance.subscriber.on("message", (channel) => {
        if (channel === "master_heartbeat") {
            lastSeen = Date.now();
        }
    });
    verifyLoop = setInterval(async() => {
        if (Date.now() - lastSeen > 10 * 1000) {
            let data = await cluster.getCluster();

            console.log(`Melo-API : Master : ${cluster.getMaster(data)} has died, i'm taking the role`);
            await cluster.setMaster(cluster);
            await cluster.removeWorker(cluster, uuid);
            cMaster.init();
        }
    }, 5000);
};

exports.stop = () => {
    clearInterval(heartbeatLoop);
    clearInterval(verifyLoop);
    RedisInstance.unSubscribe("master_heartbeat");
};
