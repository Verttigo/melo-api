exports.setIntervalAndExecute = (fn, t) => {
    fn();
    return(setInterval(fn, t));
};

exports.sleep = (ms) => {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
};
